import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from numpy import arange

numIterations = 500

df = pd.read_csv('output_data13.csv', sep=' ', header=None)
df = df.drop([0])
list_to_drop = [i for i in range(6, len(df.columns), 2)]
df = df.drop(list_to_drop, axis=1)
list_to_drop = [i for i in range(0, 4)]
df = df.drop(list_to_drop, axis=1)
column_names = ['B = ' + str(i) for i in range(0, 9)]
column_names.insert(0, 'epsilon')
df.columns = column_names
df['epsilon'] = df["epsilon"].str.replace(":", "").astype(float)

print(df)

epsilonCorrect = df.values
epsilon = epsilonCorrect[:, 0]
print(epsilon)
normalizedCorrect = epsilonCorrect[:, 1:] / numIterations
print(normalizedCorrect)
maxBudget = normalizedCorrect.shape[1]
budget = np.arange(maxBudget)
print(budget)

for i in range(0, budget.shape[0]):
    plt.plot(budget, normalizedCorrect[i], linewidth=4)
plt.xlabel('Selected sensors')
plt.ylabel('Accuracy of MAP')
plt.show()