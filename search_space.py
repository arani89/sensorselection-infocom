import numpy as np

import random

def numbers_with_sum(time, budget):
    """n numbers with sum k"""
    if time == 1:
        return [budget]
    try:
        num = random.randint(1, budget - 1)
        return [num] + numbers_with_sum(time - 1, budget - num)
    except:
        return None


def generate_sequences(budget, time, num_trials):
    sequences = np.zeros((num_trials * 50, time), dtype=int)
    cnt = 0
    for i in range(num_trials * 50):
        seq = numbers_with_sum(time, budget)
        if seq is not None:
            seq = np.array(seq)
            #np.random.shuffle(seq)
            sequences[cnt, :] = seq
            cnt += 1
    sequences = np.unique(sequences, axis=0)
    sequences = sequences[1:num_trials + 1, :]
    [np.random.shuffle(sequences[i, :]) for i in range(sequences.shape[0])]
    np.random.shuffle(sequences)
    print(sequences)
    return sequences

def seq_merge_and_split(sequence, values):
    min_pos = np.argmin(values)
    max_pos = np.argmax(values)
    time_slots = len(sequence)
    budget = np.sum(sequence)
    if sequence[max_pos] == 1: ##max_pos already has minimum no. of sensors
        return None
    elif sequence[min_pos] == budget - time_slots + 1: ##min_pos already has maximum no. of sensors
        return None

    if (min_pos > 0 and values[min_pos] + values[min_pos-1] < values[max_pos]): ##merge with left and split
        sequence[min_pos - 1] += sequence[min_pos]
        sequence[min_pos] = 0
        split1_val = sequence[max_pos] // 2
        split2_val = sequence[max_pos] - split1_val
        sequence[max_pos] = split2_val
        sequence = np.insert(sequence, max_pos, split1_val)
        sequence = np.delete(sequence, min_pos)
    elif (min_pos < time_slots - 1 and values[min_pos] + values[min_pos + 1] < values[max_pos]): ##merge with right and split
        sequence[min_pos + 1] += sequence[min_pos]
        sequence[min_pos] = 0
        split1_val = sequence[max_pos] // 2
        split2_val = sequence[max_pos] - split1_val
        sequence[max_pos] = split2_val
        sequence = np.insert(sequence, max_pos, split1_val)
        sequence = np.delete(sequence, min_pos)
    elif (values[min_pos] * 1.5 < values[max_pos]): ##Increase and decrease by 2
        sequence[min_pos] += 2
        sequence[max_pos] -= 2
    elif (values[min_pos] < values[max_pos]): ##Increase and decrease by 1
        sequence[min_pos] += 1
        sequence[max_pos] -= 1
    return sequence

sequence = np.array([7, 4, 4])
values = np.array([18, 62, 133])
diff_values = np.diff(values, n=1)
diff_values = np.insert(diff_values, 0, values[0])
new_sequence = seq_merge_and_split(sequence, diff_values)
print(new_sequence)